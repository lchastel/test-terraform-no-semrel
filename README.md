# Test Terraform No Semrel

## Create the gitlab projet in my personal space.

https://gitlab.com/lchastel/test-terraform-no-semrel.git

```
git pull git@gitlab.com:lchastel/test-terraform-no-semrel.git
```

## 1rst push

### terraform files

Simple code to get gitlab project name.

### .gitlab-ci.yml 

terraform template:
* I don't want review, integration, staging, production.
* I just enable publish.
* I change publish image because curl image is in ash not bash

### Commands 
```
cd test-terraform-no-semrel
git add .
git commit -m"feat: project creation"
git push
```

### Result of the pipeline

Success (1 job tf-tflint)

## create tag to push module

```
cd test-terraform-no-semrel
git tag -s -m"first release" 0.0.1
git push --tags
```
