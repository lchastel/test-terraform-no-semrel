output "project_name" {
  description = "Name of the gitlab project"
  value       = data.gitlab_project.my_project.name
}
