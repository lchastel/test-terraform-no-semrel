terraform {
  required_version = "1.8.3"

  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "17.0.0"
    }
  }
}
